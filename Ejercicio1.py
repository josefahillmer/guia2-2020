# !/usr/bin/env python3
# -*- coding: utf-8 -*-


def ordenar(dic):
    nuevo = []
    # Para recorrer desde el primer elemento hasta el ultimo, en dos en dos
    for i in range(0, 8, 2):
        """
        Se convierte el diccionario en str y en lista para cambiar los valores,
        de esta manera son todos srt y no str y int,
        en values se cambia desde el primer y cada dos elementos.
        """
        nuevo += str(list(dic.values())[0][i:i+2]), str(list(dic.keys())[0])
    print(nuevo)


# Función principal o main
if __name__ == "__main__":
    dic = {'Histidine': 'C6H9N3O2'}
    ordenar(dic)
