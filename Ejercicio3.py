# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import json


# Procedimiento para enviar datos al archivo y guardarlos
def save_file(aa):
    with open('archivo.json', 'w') as nuevo:
        json.dump(aa, nuevo)


# Procedieminto para cargar el archivo y transformarlo en lenguaje python
def load_file():
    with open("archivo.json", 'r') as nuevo:
        aa = json.load(nuevo)
    return aa


# Procedimiento para encontrar aminoacidos por sus atomos
def dividir(aa, busqueda):

    encontrado = []
    # Se convierte todo en mayuscula
    busqueda = busqueda.upper()
    # Recorre todos los valores de aa y si encuentra valor lo guarda en key
    for key, value in aa.items():
        if busqueda in value:
            encontrado.append(key)
    print("Aminoacidos encontrados: ", encontrado)


# Procedimiento para buscar el aminaocido que el usuario quiere
def buscar(aa, busqueda):

    # Si encuentra resultado, sera busqueda, si no es asi sera false
    resultado = aa.get(busqueda, False)

    if resultado:
        # Se encuentra busqueda y se imprime
        if busqueda in aa:
            print("Aminoacido encontrado:{}-{}".format(busqueda, aa[busqueda]))
    # Busqueda tiene que ser menor a 5 por el tamaño de la formula molecular
    if len(busqueda) < 5:
        # Llama a la funcion dividir
        dividir(aa, busqueda)
    else:
        print("El aminoacido {} no existe".format(busqueda))

    return resultado


# Para buscar si el aminoacido esta en el archivo
def buscar_2(aa, busqueda):

    resultado = aa.get(busqueda, False)

    if resultado:
        print("El aminoacido {} fue encontrado".format(busqueda))

    return resultado


# Procedimiento para ingresar un aminoacido
def ingresar(aa):

    aa = load_file()

    aminoacido = input("Ingrese el nombre del aminoacido: ")

    existe = buscar_2(aa, aminoacido)
    # Si no existe se agregara uno nuevo, si existe dira que ya esta
    if not existe:
        # Se agregan 20 aminoacidos
        for i in range(20):
            aminoacido = input("Ingrese el nombre del aminoacido: ")
            formula_molecular = input("Ingrese la formula molecular del aa: ")
            aa[aminoacido] = formula_molecular
    else:
        print("El aminoacido {} ya existe".format(aminoacido))

    save_file(aa)


# Procedimiento para reemplazar un aminoacido
def reemplazo(aa):

    aa = load_file()

    editar = input("Ingrese el aminoacido que desea modificar: ")
    # Buca el aminoacido en aa
    existe = buscar_2(aa, editar)
    # Si existe se editara
    if existe:
        aminoacido = editar
        formula_molecular = input("Ingrese la nueva formula molecular del aa: ")
        aa[aminoacido] = formula_molecular

        save_file(aa)


# Procedimiento para eliminar aminoacidos
def eliminar():

    aa = load_file()
    aminoacido = input("Ingrese el aminoacido a eliminar: ")

    existe = buscar_2(aa, aminoacido)
    # Si existe se eliminara el aminoacido
    if existe:
        del aa[aminoacido]
        save_file(aa)

    else:
        print("El elemento no existe")


# Procedimiento que imprimi el total de aminoacidos guardados
def total(aa):
    texto = "Aminoácido - Formula Molecular"
    print("Aminoácido - Formula Molecular".format(texto))
    for key, value in aa.items():
        print("{0} - {1}".format(key, value))


# Procedimiento para menu
def menu():

    aa = {}
    while True:
        print("Presione:")
        print("1. Ingresar aminoacidos")
        print("2. Buscar aminoacidos")
        print("3. Reemplazar aminoacidos")
        print("4. Eliminar aminoacidos")
        print("5. Ver todos los aminoacidos")
        print("0. Salir")
        opcion = input("Ingrese una opción: ")

        if opcion == "1":
            print("Va a ingresar 20 aminoacidos")
            aa = ingresar(aa)

        elif opcion == "2":
            aa = load_file()
            busqueda = input("Ingrese lo que desea buscar: ")
            buscar(aa, busqueda)

        elif opcion == "3":
            aa = reemplazo(aa)

        elif opcion == "4":
            eliminar()

        elif opcion == "5":
            aa = load_file()
            total(aa)

        elif opcion == "0":
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass


# Función principal o main
if __name__ == "__main__":
    menu()

