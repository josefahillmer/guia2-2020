# !/usr/bin/env python3
# -*- coding: utf-8 -*-


# Procedimiento para abrir el archivo y hacerle modificacione
def abrir():
    archivo = open("covid_19_data.csv")
    datos = []
    for i, orden in enumerate(archivo):
        # Se ordena la lista por comas y se sacan los decimales
        orden = orden.replace('.0', '').split(',')
        # Ultima fecha de actualizacion
        if orden[1] == '03/18/2020':
            datos.append(orden)
    archivo.close()
    return datos


# Procedimiento para sacar el total de paises que tienen contagiados
def total(datos):
    cont = 0
    for i in range(len(datos)):
        # Para quitar los paises repetidos y sumar el total de estos
        if len(set(datos[i][3])) > len(datos[i][2]):
            cont += 1
    return cont


# Función principal o main
if __name__ == "__main__":

    datos = abrir()
    cont = total(datos)
    # Procedimiento para imprimir las estadisticas
    for i in range(len(datos)):
        print("Pais: ", datos[i][3], "Infectados:", datos[i][5],
            "Recuperados + Muertos:", datos[i][6] + datos [i][7])
    print("Cantidad total de paises que tienen contagiados", cont)

